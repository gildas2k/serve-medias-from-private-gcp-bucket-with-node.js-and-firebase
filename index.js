'use strict';

require('module-alias/register');
require('dotenv').config();

const firebaseService = require('@src/services/firebase/init.service');
firebaseService.init();

const app = require('express')();
require('@src/router').init(app);
require('@src/http-server').start(app);
