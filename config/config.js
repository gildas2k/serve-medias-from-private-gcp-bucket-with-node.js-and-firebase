'use strict';

const config = require('@config/config.json');

try {
    config.firebase.serviceAccount = require(process.env.FIREBASE_SERVICE_ACCOUNT);
} catch (e) {
    config.firebase.serviceAccount = {};
}

module.exports = config;
