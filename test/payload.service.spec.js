'use strict';

require('module-alias/register');
const { expect } = require('chai');
const payloadService = require('@src/services/payload.service');

describe('Payload service', function () {
    describe('validate()', function () {
        it('should fail if args are wrong', function () {
            // Given
            const schemaName = 'getMediaByFilename';
            const values = { name: 'test.jpg' };
            // Then
            expect(payloadService.validate(schemaName, values)).to.be.false;
        });

        it('should pass if args are right', function () {
            // Given
            const schemaName = 'getMediaByFilename';
            const values = { filename: 'test.jpg' };
            // Then
            expect(payloadService.validate(schemaName, values)).to.be.true;
        });
    });
});
