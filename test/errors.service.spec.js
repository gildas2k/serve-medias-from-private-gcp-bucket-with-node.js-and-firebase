'use strict';

require('module-alias/register');
const { expect } = require('chai');
const errorsService = require('@src/services/errors.service');

describe('Error service', function () {
    describe('getHttpCode()', function () {
        it('should return 404 when not_found', function () {
            expect(errorsService.getHttpCode('not_found')).to.equal(404);
        });

        it('should return 403 when forbidden', function () {
            expect(errorsService.getHttpCode('forbidden')).to.equal(403);
        });

        it('should return 409 when conflict', function () {
            expect(errorsService.getHttpCode('conflict')).to.equal(409);
        });

        it('should return 500 by default', function () {
            expect(errorsService.getHttpCode('test')).to.equal(500);
            expect(errorsService.getHttpCode()).to.equal(500);
        });
    });
});
