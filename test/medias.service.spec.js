'use strict';

require('module-alias/register');
const config = require('@config/config');
const { assert, expect } = require('chai');
const { stub } = require('sinon');
const mediasService = require('@src/services/medias.service');

describe('Medias service', function () {
    let generateSignedUrl;
    const bucketName = 'bucket';
    const filename = 'file.jpg';

    before(() => {
        generateSignedUrl = stub(mediasService, 'generateSignedUrl').callsFake(() => ({
            url: 'generated',
            expires: 'expires'
        }));
    });

    describe('getMediaName()', function () {
        it('should return document name correctly formatted', async function () {
            // When
            const documentName = mediasService.getMediaName(bucketName, filename);
            // Then
            expect(documentName).to.equals('bucket-file.jpg');
        });
    });

    describe('isUrlExpired()', function () {
        it('should return true if url is expired', async function () {
            // Given
            const yesterday = new Date();
            yesterday.setDate(yesterday.getDate() - 1);
            // When
            const isExpired = mediasService._firebaseStorage.isUrlExpired(yesterday.valueOf());
            // Then
            expect(isExpired).to.be.true;
        });

        it('should return false if url is not expired', async function () {
            // Given
            const tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            // When
            const isExpired = mediasService._firebaseStorage.isUrlExpired(tomorrow.valueOf());
            // Then
            expect(isExpired).to.be.false;
        });
    });

    describe('getSignedUrl()', function () {
        it('should return cached url if cached', async function () {
            // Given
            stub(mediasService._firebaseStorage, 'setInstance').callsFake(() => {
                mediasService._firebaseStorage.instance = {
                    fetch: () => ({
                        exists: true,
                        data: () => ({ url: 'cached', expires: Date.now() })
                    }),
                    add: () => {}
                };
            });
            // When
            const url = await mediasService.getSignedUrl(bucketName, filename);
            // Then
            expect(url).to.equals('cached');
            mediasService._firebaseStorage.setInstance.restore();
        });

        it('should return generated url if no cache', async function () {
            // Given
            stub(mediasService._firebaseStorage, 'setInstance').callsFake(() => {
                mediasService._firebaseStorage.instance = {
                    fetch: () => ({}),
                    add: () => {}
                };
            });
            // When
            const url = await mediasService.getSignedUrl(bucketName, filename);
            // Then
            assert(
                generateSignedUrl.calledWith(bucketName, filename, config.gcp.expirationDelay),
                '"generateSignedUrl" not properly called in getSignedUrl'
            );
            expect(url).to.equals('generated');
            mediasService._firebaseStorage.setInstance.restore();
        });

        it('should return generated url if document does not exist', async function () {
            // Given
            stub(mediasService._firebaseStorage, 'setInstance').callsFake(() => {
                mediasService._firebaseStorage.instance = {
                    fetch: () => ({
                        exists: false
                    }),
                    add: () => {}
                };
            });
            // When
            const url = await mediasService.getSignedUrl(bucketName, filename);
            // Then
            expect(url).to.equals('generated');
            mediasService._firebaseStorage.setInstance.restore();
        });

        it('should return generated url if url is expired', async function () {
            // Given
            stub(mediasService._firebaseStorage, 'setInstance').callsFake(() => {
                const yesterday = new Date();
                yesterday.setDate(yesterday.getDate() - 1);
                mediasService._firebaseStorage.instance = {
                    fetch: () => ({
                        exists: true,
                        data: () => ({ url: 'cached', expires: yesterday.valueOf() })
                    }),
                    add: () => {}
                };
            });
            // When
            const url = await mediasService.getSignedUrl(bucketName, filename);
            // Then
            expect(url).to.equals('generated');
            mediasService._firebaseStorage.setInstance.restore();
        });
    });
});
