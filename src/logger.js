'use strict';

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;

const fileLogFormat = printf(info => {
    return `${info.timestamp} ${info.level}: ${info.message}`;
});

const consoleLogFormat = printf(info => {
    return `[ ${info.level.toUpperCase()} ] ${info.message}`;
});

module.exports = {
    init() {
        const logger = createLogger({
            format: combine(timestamp(), fileLogFormat),
            transports: [new transports.Console({ format: consoleLogFormat })]
        });

        return logger;
    }
};
