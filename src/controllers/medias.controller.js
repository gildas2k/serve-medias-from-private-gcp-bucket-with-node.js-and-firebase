'use strict';

const config = require('@config/config');
const express = require('express');
const router = express.Router();
const logger = require('@src/logger').init();
const mediasService = require('@src/services/medias.service');
const payloadService = require('@src/services/payload.service');
const errorsService = require('@src/services/errors.service');

router.get('/:filename', async (req, res) => {
    if (!payloadService.validate('getMediaByFilename', req.params)) {
        return res.status(400).end();
    }

    try {
        const signedMediaUrl = await mediasService.getSignedUrl(
            config.gcp.privateMediasBucketName,
            req.params.filename
        );

        if (!signedMediaUrl) {
            return res.status(404).end();
        }

        res.redirect(signedMediaUrl);
    } catch (e) {
        logger.error(`${req.method} ${req.baseUrl} => ${e}`);
        res.status(errorsService.getHttpCode(e.message)).end();
    }
});

module.exports = router;
