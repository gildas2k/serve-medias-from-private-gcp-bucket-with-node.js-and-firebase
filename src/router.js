'use strict';

const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

module.exports = {
    init(app) {
        app.use(cors());
        app.use(morgan('combined'));

        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());

        app.use('/medias', require('@src/controllers/medias.controller'));
    }
};
