'use strict';

const config = require('@config/config');
const logger = require('@src/logger').init();

module.exports = {
    start(app) {
        app.listen(config.server.port, config.server.ip, () => {
            logger.info(`Listening on ${config.server.ip}:${config.server.port}`);
        });
    }
};
