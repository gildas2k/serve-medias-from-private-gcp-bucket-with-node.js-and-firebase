'use strict';

module.exports = {
    getHttpCode(errorMessage) {
        switch (errorMessage) {
            case 'not_found':
                return 404;
            case 'forbidden':
                return 403;
            case 'conflict':
                return 409;
            default:
                return 500;
        }
    }
};
