'use strict';

const admin = require('firebase-admin');

class FirebaseStorageService {
    constructor(collectionName) {
        this.db = admin.firestore();
        this.collection = this.db.collection(collectionName);
    }

    fetch(documentName) {
        return this.collection.doc('' + documentName).get();
    }

    add(documentName, data) {
        return this.collection.doc('' + documentName).set(data);
    }
}

module.exports = FirebaseStorageService;
