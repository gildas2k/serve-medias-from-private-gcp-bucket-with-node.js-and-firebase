'use strict';

const admin = require('firebase-admin');
const config = require('@config/config');

module.exports = {
    init() {
        admin.initializeApp({
            credential: admin.credential.cert(config.firebase.serviceAccount),
            databaseURL: `https://${config.firebase.database}.firebaseio.com`
        });
    }
};
