'use strict';

const config = require('@config/config');
const slugify = require('slugify');
const FirebaseStorageService = require('@src/services/firebase/storage.service');
const { Storage } = require('@google-cloud/storage');
const storage = new Storage();

module.exports = {
    _firebaseStorage: {
        instance: null,
        setInstance() {
            this.instance = new FirebaseStorageService(config.firebase.collection);
        },

        fetch(documentName) {
            return this.instance.fetch(documentName);
        },

        add(documentName, url, expires) {
            this.instance.add(documentName, { url, expires });
        },

        checkUrl(document) {
            if (document.exists && !this.isUrlExpired(document.data().expires)) {
                // Return cache url if exists
                return document.data().url;
            }

            return;
        },

        isUrlExpired(timestamp) {
            return Date.now() > timestamp;
        }
    },

    getMediaName(bucketName, filename) {
        return slugify(bucketName + ' ' + filename);
    },

    async getSignedUrl(bucketName, filename) {
        const documentName = this.getMediaName(bucketName, filename);
        this._firebaseStorage.setInstance();
        try {
            // Check if cache url already exists
            const document = await this._firebaseStorage.fetch(documentName);
            const unexpiredUrl = this._firebaseStorage.checkUrl(document);
            if (unexpiredUrl) {
                // Return cache url if exists
                return unexpiredUrl;
            }

            // Generate new signed url if no cached url
            const { url, expires } = await this.generateSignedUrl(bucketName, filename, config.gcp.expirationDelay);

            // Create cache url
            this._firebaseStorage.add(documentName, url, expires);

            // Return generated url
            return url;
        } catch (e) {
            console.error(e);
        }
    },

    async generateSignedUrl(bucketName, filename, expirationDelay) {
        const expires = Date.now() + expirationDelay;
        const options = {
            version: 'v4',
            action: 'read',
            expires
        };

        // Generate signed url with google apis
        const [url] = await storage.bucket(bucketName).file(filename).getSignedUrl(options);

        return { url, expires };
    }
};
