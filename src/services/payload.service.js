'use strict';

const Ajv = require('ajv');
const schemas = require('@config/schemas.json');

module.exports = {
    validate(schemaName, values) {
        const ajv = new Ajv();

        ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-06.json'));

        return ajv.validate(schemas[schemaName], values);
    }
};
