# Serve medias from private GCP bucket with Node.js and Firebase

This project aim to serve medias securely from a private GCP bucket with Node.js.

It uses firebase to create a cache to avoid generating new signed url for a file, while there is a previous not expire one.

## Before you begin
- Create GCP project by following the tutorial : https://gitlab.com/gildas2k/serve-medias-from-private-gcp-bucket-with-node.js-and-firebase/-/wikis/Initialize-GCP-project
- Copy .env-template into .env
- Open .env file and fill json service account key full path for GOOGLE_APPLICATION_CREDENTIALS.
    
    Use slashes instead of backslashes

    Ex : C:/Users/xxx/path/to/my/key.json

- Create firebase project by following the tutorial : https://gitlab.com/gildas2k/serve-medias-from-private-gcp-bucket-with-node.js-and-firebase/-/wikis/Initialize-Firebase-project
- Open .env file and fill firebase service account key full path for FIREBASE_SERVICE_ACCOUNT.

## Install

```bash
npm i
```

## Start
```bash
node index.js
```

## Run tests
```bash
npm test
```
